/** @file
 *
 * This file implements unit test for inode operations
 */

#include <inode.h>
#include <stmpfs.h>
#include <pathname_t.h>

int main()
{
    try
    {
        inode_t inode;
        inode.fs_stat.st_size = 114514;
        inode.add_dentry("root", inode);
        auto &inode2 = pathname_to_inode(stmpfs_pathname_t("/root/root/root"), inode);

        if (inode2.fs_stat.st_size == 114514)
        {
            return 0;
        }
        else
        {
            return 1;
        }
    }
    catch (...)
    {
        return 1;
    }
}
