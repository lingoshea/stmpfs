/** @file
 *
 * This file implements unit test for inode operations
 */

#include <inode.h>
#include <cstring>

int main()
{
    inode_t inode;
    char buff[1024];
    memset(buff, '0', sizeof(buff));
    inode.write(buff, 4, 0);

    memset(buff, '1', sizeof(buff));
    inode.write(buff, 4, 2);

    memset(buff, '0', sizeof(buff));
    inode.read(buff, 4, 1);

    if (!!memcmp(buff, "0111", 4))
    {
        return 1;
    }

    inode_t inode2;
    inode2.write("ABC", 3, 0);
    inode2.replicate_inode();
    inode2.write("BCD", 3, 1);
    inode2.read(buff, sizeof(buff), 0);

    if (!!memcmp(buff, "ABCD", 4))
    {
        return 1;
    }

    inode2.fs_stat.st_mode = S_IFREG | 0755;
    inode.emplace_new_dentry("file1", inode2);
    inode.del_dentry("file1");
    inode.add_dentry("root", inode);

    inode.clear();

    return 0;
}
