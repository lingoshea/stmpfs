/** @file
 *
 * This file implements unit test for pathname resolver
 */

#include <pathname_t.h>

int main()
{
    stmpfs_pathname_t path("/tmp/tmp/tmp");

    for (auto & i : path.get_direct_pathname())
    {
        if (i != "tmp")
        {
            return 1;
        }
    }

    return 0;
}
