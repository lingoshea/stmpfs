/** @file
 *
 * This file implements the APIs for FUSE
 */

#define FUSE_USE_VERSION 31
#include <fuse.h>
#include <sys/types.h>
#include <pathname_t.h>
#include <inode.h>
#include <stmpfs_error.h>
#include <iostream>
#include <stmpfs.h>
#include <fuse_ops.h>
#include <execinfo.h>
#include <sys/xattr.h>

#define MIN(a, b) ((a) < (b) ? (a) : (b))

#ifdef CMAKE_BUILD_DEBUG
# define BACKTRACE_SZ 64    /* Stack frame size */
/// Obtain stack frame
# define OBTAIN_STACK_FRAME                             \
{                                                       \
    void *__array[BACKTRACE_SZ];                        \
    int __size, __i;                                    \
    char **__strings;                                   \
    __size = backtrace (__array, BACKTRACE_SZ);         \
    __strings = backtrace_symbols (__array, __size);    \
                                                        \
    if (__strings != nullptr)                           \
    {                                                   \
        std::cerr   << std::endl                        \
                    << "Obtained stack frame(s):"       \
                    << std::endl;                       \
        for (__i = 0; __i < __size; __i++)              \
        {                                               \
            std::cerr << "\t" << __strings[__i] << "\n";\
        }                                               \
        free (__strings);                               \
    }                                                   \
} __asm__("nop") /* suppress IDE "empty statement" warning */

# define FUNCTION_INFO                                  \
{                                                       \
    std::cerr << "\nFrom " << __FILE__ << ":"           \
              << __LINE__ << ": "                       \
              << __FUNCTION__ << ": " << path << ":\n"; \
} __asm__("nop") /* suppress IDE "empty statement" warning */

#else // CMAKE_BUILD_DEBUG
# define OBTAIN_STACK_FRAME __asm__("nop") /* suppress IDE "empty statement" warning */
# define FUNCTION_INFO      __asm__("nop") /* suppress IDE "empty statement" warning */
#endif // CMAKE_BUILD_DEBUG

/// filesystem root
inode_t filesystem_root;

/// if the given vpath is the replicate entry dir
/** @param vpath pathname **/
inline bool if_is_replicate_path(stmpfs_pathname_t & vpath)
{
    return (vpath.get_direct_pathname().size() == 1 &&
            vpath.get_pathname().front() == REPLICATE_DIR_NAME);
}

/// if the given vpath is in the replicate entry dir
/** @param vpath pathname **/
inline bool if_in_replicate_path(stmpfs_pathname_t & vpath)
{
    return (!vpath.get_direct_pathname().empty() &&
            vpath.get_pathname().front() == REPLICATE_DIR_NAME);
}

/// erase front pathname in given vpath
/** @param vpath pathname **/
inline void vpath_erase_front(stmpfs_pathname_t & vpath)
{
    if (!vpath.get_direct_pathname().empty()) {
        vpath.get_direct_pathname().erase(vpath.get_direct_pathname().begin());
    }
}

/// if this inode is replicated
inline bool if_replicated(inode_t & inode)
{
    return inode.replicated.if_replicated;
}

int do_getattr (const char *path, struct stat *stbuf)
{
    try
    {
        FUNCTION_INFO;

        stmpfs_pathname_t vpath(path);

        if (if_in_replicate_path(vpath))
        {
            vpath_erase_front(vpath);
            auto &inode = pathname_to_inode(vpath, filesystem_root);
            if (if_replicated(inode))
            {
                *stbuf = inode.replicated.fs_stat;
            }
            else
            {
                return -ENOENT;     // No such file or directory (POSIX.1-2001)
            }
        }
        else
        {
            auto &inode = pathname_to_inode(vpath, filesystem_root);
            if (!inode.if_deleted_in_current_version)
            {
                *stbuf = inode.fs_stat;
            }
            else
            {
                return -ENOENT;     // No such file or directory (POSIX.1-2001)
            }
        }

        return 0;
    }
    catch (stmpfs_error_t & error)
    {
        OBTAIN_STACK_FRAME;
        std::cerr << error.what() << " (errno=" << error.what_errno() << ")" << std::endl;
        if (error.my_errcode() == STMPFS_ERROR_NO_SUCH_FILE_OR_DIRECTORY)
        {
            errno = ENOENT; // No such file or directory (POSIX.1-2001)
        }
        return -errno;
    }
    catch (std::exception & error)
    {
        OBTAIN_STACK_FRAME;
        std::cerr << error.what() << " (errno=" << strerror(errno) << ")" << std::endl;
        return -errno;
    }
}

int do_readdir (const char *path,
                void *buffer,
                fuse_fill_dir_t filler,
                off_t,
                struct fuse_file_info *)
{
    try
    {
        FUNCTION_INFO;

        stmpfs_pathname_t vpath(path);

        filler(buffer, ".", nullptr, 0);  // Current Directory
        filler(buffer, "..", nullptr, 0); // Parent Directory

        if (if_in_replicate_path(vpath))
        {
            vpath_erase_front(vpath);
            auto &inode = pathname_to_inode(vpath, filesystem_root);

            for (auto & i: inode.my_dentry())
            {
                if (if_replicated(*i.second.inode))
                {
                    filler(buffer, i.first.c_str(), nullptr, 0);
                }
            }
        }
        else
        {
            if (vpath.get_direct_pathname().empty())
            {
                filler(buffer, REPLICATE_DIR_NAME, nullptr, 0); // replicate entry
            }

            // normal read
            auto &inode = pathname_to_inode(vpath, filesystem_root);
            inode.fs_stat.st_atim = current_time();

            for (auto & i: inode.my_dentry())
            {
                if (!i.second.inode->if_deleted_in_current_version)
                {
                    filler(buffer, i.first.c_str(), nullptr, 0);
                }
            }
        }

        return 0;
    }
    catch (stmpfs_error_t & error)
    {
        OBTAIN_STACK_FRAME;
        std::cerr << error.what() << " (errno=" << error.what_errno() << ")" << std::endl;
        if (error.my_errcode() == STMPFS_ERROR_NO_SUCH_FILE_OR_DIRECTORY)
        {
            errno = ENOENT; // No such file or directory (POSIX.1-2001)
        }
        return -errno;
    }
    catch (std::exception & error)
    {
        OBTAIN_STACK_FRAME;
        std::cerr << error.what() << " (errno=" << strerror(errno) << ")" << std::endl;
        return -errno;
    }
}

int do_mkdir (const char * path, mode_t mode)
{
    try
    {
        FUNCTION_INFO;

        stmpfs_pathname_t vpath(path);

        // replicate
        if (if_is_replicate_path(vpath))
        {
            filesystem_root.replicate_clear();
            filesystem_root.replicate_inode();
            return 0;
        }

        if (if_in_replicate_path(vpath))
        {
            return -EROFS;  // Read-only filesystem (POSIX.1-2001)
        }

        // get target name
        std::string tag_name = vpath.get_direct_pathname().back();
        vpath.get_direct_pathname().pop_back();

        auto & inode = pathname_to_inode(vpath, filesystem_root);
        auto tag_inode = inode.my_dentry().find(tag_name);

        inode_t new_inode;
        auto cur_time = current_time();
        new_inode.fs_stat.st_mode = mode | S_IFDIR;
        new_inode.fs_stat.st_atim = cur_time;
        new_inode.fs_stat.st_ctim = cur_time;
        new_inode.fs_stat.st_mtim = cur_time;

        if (tag_inode != inode.my_dentry().end())
        {
            if (tag_inode->second.inode->if_deleted_in_current_version)
            {
                tag_inode->second.inode->fs_stat = new_inode.fs_stat;
                tag_inode->second.inode->my_dentry().clear();
                tag_inode->second.inode->if_deleted_in_current_version = false;
            }
            else // target exists
            {
                return -1;
            }
        }
        else
        {
            inode.emplace_new_dentry(tag_name, new_inode);
        }

        return 0;
    }
    catch (stmpfs_error_t & error)
    {
        OBTAIN_STACK_FRAME;
        std::cerr << error.what() << " (errno=" << error.what_errno() << ")" << std::endl;
        if (error.my_errcode() == STMPFS_ERROR_NO_SUCH_FILE_OR_DIRECTORY)
        {
            errno = ENOENT; // No such file or directory (POSIX.1-2001)
        }
        return -errno;
    }
    catch (std::exception & error)
    {
        OBTAIN_STACK_FRAME;
        std::cerr << error.what() << " (errno=" << strerror(errno) << ")" << std::endl;
        return -errno;
    }
}

int do_chmod (const char * path, mode_t mode)
{
    try
    {
        FUNCTION_INFO;

        stmpfs_pathname_t vpath(path);

        if (if_in_replicate_path(vpath))
        {
            return -EROFS;  // Read-only filesystem (POSIX.1-2001)
        }

        auto & inode = pathname_to_inode(vpath, filesystem_root);
        if (!inode.if_deleted_in_current_version)
        {
            inode.fs_stat.st_mode = mode;
        }
        else
        {
            return -ENOENT;     // No such file or directory (POSIX.1-2001)
        }

        return 0;
    }
    catch (stmpfs_error_t & error)
    {
        OBTAIN_STACK_FRAME;
        std::cerr << error.what() << " (errno=" << error.what_errno() << ")" << std::endl;
        if (error.my_errcode() == STMPFS_ERROR_NO_SUCH_FILE_OR_DIRECTORY)
        {
            errno = ENOENT; // No such file or directory (POSIX.1-2001)
        }
        return -errno;
    }
    catch (std::exception & error)
    {
        OBTAIN_STACK_FRAME;
        std::cerr << error.what() << " (errno=" << strerror(errno) << ")" << std::endl;
        return -errno;
    }
}

int do_chown (const char * path, uid_t uid, gid_t gid)
{
    try
    {
        FUNCTION_INFO;

        stmpfs_pathname_t vpath(path);

        if (if_in_replicate_path(vpath))
        {
            return -EROFS;  // Read-only filesystem (POSIX.1-2001)
        }

        auto & inode = pathname_to_inode(vpath, filesystem_root);
        if (!inode.if_deleted_in_current_version)
        {
            inode.fs_stat.st_uid = uid;
            inode.fs_stat.st_gid = gid;
        }
        else
        {
            return -ENOENT;     // No such file or directory (POSIX.1-2001)
        }

        return 0;
    }
    catch (stmpfs_error_t & error)
    {
        OBTAIN_STACK_FRAME;
        std::cerr << error.what() << " (errno=" << error.what_errno() << ")" << std::endl;
        if (error.my_errcode() == STMPFS_ERROR_NO_SUCH_FILE_OR_DIRECTORY)
        {
            errno = ENOENT; // No such file or directory (POSIX.1-2001)
        }
        return -errno;
    }
    catch (std::exception & error)
    {
        OBTAIN_STACK_FRAME;
        std::cerr << error.what() << " (errno=" << strerror(errno) << ")" << std::endl;
        return -errno;
    }
}

int do_create (const char * path, mode_t mode, struct fuse_file_info *)
{
    try
    {
        FUNCTION_INFO;

        stmpfs_pathname_t vpath(path);

        if (if_in_replicate_path(vpath))
        {
            return -EROFS;  // Read-only filesystem (POSIX.1-2001)
        }

        // get target name
        std::string tag_name = vpath.get_direct_pathname().back();
        vpath.get_direct_pathname().pop_back();

        auto & inode = pathname_to_inode(vpath, filesystem_root);
        auto tag_inode = inode.my_dentry().find(tag_name);

        inode_t new_inode;

        // fill up info
        auto cur_time = current_time();
        new_inode.fs_stat.st_mode = mode;
        new_inode.fs_stat.st_nlink = 1;
        new_inode.fs_stat.st_atim = cur_time;
        new_inode.fs_stat.st_ctim = cur_time;
        new_inode.fs_stat.st_mtim = cur_time;

        if (tag_inode != inode.my_dentry().end())
        {
            if (tag_inode->second.inode->if_deleted_in_current_version)
            {
                tag_inode->second.inode->my_data().clear();
                tag_inode->second.inode->fs_stat = new_inode.fs_stat;
                tag_inode->second.inode->if_deleted_in_current_version = false;
            }
            else // target exists
            {
                return -EEXIST; // File exists (POSIX.1-2001).
            }
        }
        else
        {
            inode.emplace_new_dentry(tag_name, new_inode);
        }

        return 0;
    }
    catch (stmpfs_error_t & error)
    {
        OBTAIN_STACK_FRAME;
        std::cerr << error.what() << " (errno=" << error.what_errno() << ")" << std::endl;
        if (error.my_errcode() == STMPFS_ERROR_NO_SUCH_FILE_OR_DIRECTORY)
        {
            errno = ENOENT; // No such file or directory (POSIX.1-2001)
        }
        return -errno;
    }
    catch (std::exception & error)
    {
        OBTAIN_STACK_FRAME;
        std::cerr << error.what() << " (errno=" << strerror(errno) << ")" << std::endl;
        return -errno;
    }
}

int do_flush (const char * path, struct fuse_file_info *)
{
    FUNCTION_INFO;
    return 0;
}

int do_release (const char * path, struct fuse_file_info *)
{
    FUNCTION_INFO;
    return 0;
}

int do_open (const char * path, struct fuse_file_info *)
{
    try
    {
        FUNCTION_INFO;

        stmpfs_pathname_t vpath(path);

        if (if_in_replicate_path(vpath))
        {
            vpath_erase_front(vpath);
            inode_t & inode = pathname_to_inode(vpath, filesystem_root);
            if (if_replicated(inode))
            {
                return 0;
            }
            else
            {
                return -ENOENT; // No such file or directory (POSIX.1-2001);
            }
        }
        else
        {
            inode_t &inode = pathname_to_inode(vpath, filesystem_root);
            if (!inode.if_deleted_in_current_version)
            {
                inode.fs_stat.st_atim = current_time();
                return 0;
            }
            else
            {
                return -ENOENT; // No such file or directory (POSIX.1-2001);
            }
        }
    }
    catch (stmpfs_error_t & error)
    {
        OBTAIN_STACK_FRAME;
        std::cerr << error.what() << " (errno=" << error.what_errno() << ")" << std::endl;
        if (error.my_errcode() == STMPFS_ERROR_NO_SUCH_FILE_OR_DIRECTORY)
        {
            errno = ENOENT; // No such file or directory (POSIX.1-2001)
        }
        return -errno;
    }
    catch (std::exception & error)
    {
        OBTAIN_STACK_FRAME;
        std::cerr << error.what() << " (errno=" << strerror(errno) << ")" << std::endl;
        return -errno;
    }
}

int do_read (const char *path,
             char *buffer,
             size_t size,
             off_t offset,
             struct fuse_file_info *)
{
    try
    {
        FUNCTION_INFO;

        stmpfs_pathname_t vpath(path);

        if (if_in_replicate_path(vpath))
        {
            vpath_erase_front(vpath);
            inode_t & inode = pathname_to_inode(vpath, filesystem_root);
            if (if_replicated(inode))
            {
                return (int)inode.replicate_read(buffer, size, offset);
            }
            else
            {
                return -ENOENT; // No such file or directory (POSIX.1-2001);
            }
        }
        else
        {
            auto &inode = pathname_to_inode(vpath, filesystem_root);

            if (inode.if_deleted_in_current_version)
            {
                return -ENOENT;
            }

            inode.fs_stat.st_atim = current_time();
            return (int) inode.read(buffer, size, offset);
        }
    }
    catch (stmpfs_error_t & error)
    {
        OBTAIN_STACK_FRAME;
        std::cerr << error.what() << " (errno=" << error.what_errno() << ")" << std::endl;
        if (error.my_errcode() == STMPFS_ERROR_NO_SUCH_FILE_OR_DIRECTORY)
        {
            errno = ENOENT; // No such file or directory (POSIX.1-2001)
        }
        return -errno;
    }
    catch (std::exception & error)
    {
        OBTAIN_STACK_FRAME;
        std::cerr << error.what() << " (errno=" << strerror(errno) << ")" << std::endl;
        return -errno;
    }
}

int do_write (const char * path, const char * buffer, size_t size, off_t offset,
             struct fuse_file_info *)
{
    try
    {
        FUNCTION_INFO;

        stmpfs_pathname_t vpath(path);

        if (if_in_replicate_path(vpath))
        {
            return -EROFS;  // Read-only filesystem (POSIX.1-2001)
        }

        auto & inode = pathname_to_inode(vpath, filesystem_root);

        if (inode.if_deleted_in_current_version)
        {
            return -ENOENT;
        }

        inode.fs_stat.st_ctim = current_time();
        return (int)inode.write(buffer, size, offset);
    }
    catch (stmpfs_error_t & error)
    {
        OBTAIN_STACK_FRAME;
        std::cerr << error.what() << " (errno=" << error.what_errno() << ")" << std::endl;
        if (error.my_errcode() == STMPFS_ERROR_NO_SUCH_FILE_OR_DIRECTORY)
        {
            errno = ENOENT; // No such file or directory (POSIX.1-2001)
        }
        return -errno;
    }
    catch (std::exception & error)
    {
        OBTAIN_STACK_FRAME;
        std::cerr << error.what() << " (errno=" << strerror(errno) << ")" << std::endl;
        return -errno;
    }
}

int do_utimens (const char * path, const struct timespec tv[2])
{
    try
    {
        FUNCTION_INFO;

        stmpfs_pathname_t vpath(path);

        if (if_in_replicate_path(vpath))
        {
            return -EROFS;  // Read-only filesystem (POSIX.1-2001)
        }

        auto & inode = pathname_to_inode(vpath, filesystem_root);

        if (inode.if_deleted_in_current_version)
        {
            return -ENOENT;
        }

        inode.fs_stat.st_atim = tv[0];
        inode.fs_stat.st_mtim = tv[1];

        return 0;
    }
    catch (stmpfs_error_t & error)
    {
        OBTAIN_STACK_FRAME;
        std::cerr << error.what() << " (errno=" << error.what_errno() << ")" << std::endl;
        if (error.my_errcode() == STMPFS_ERROR_NO_SUCH_FILE_OR_DIRECTORY)
        {
            errno = ENOENT; // No such file or directory (POSIX.1-2001)
        }
        return -errno;
    }
    catch (std::exception & error)
    {
        OBTAIN_STACK_FRAME;
        std::cerr << error.what() << " (errno=" << strerror(errno) << ")" << std::endl;
        return -errno;
    }
}

int do_unlink (const char * path)
{
    try
    {
        FUNCTION_INFO;

        stmpfs_pathname_t vpath(path);

        if (if_in_replicate_path(vpath))
        {
            return -EROFS;  // Read-only filesystem (POSIX.1-2001)
        }

        // attempt to delete root
        if (vpath.get_direct_pathname().empty())
        {
            return -EISDIR; // Is a directory (POSIX.1-2001).
        }

        std::string tag_name = vpath.get_direct_pathname().back();
        vpath.get_direct_pathname().pop_back();

        auto & inode = pathname_to_inode(vpath, filesystem_root);
        auto * target_inode = inode.find_in_dentry(tag_name);

        if (target_inode->replicated.if_replicated)
        {
            target_inode->if_deleted_in_current_version = true;
        }
        else
        {
            inode.del_dentry(tag_name);
        }
//        if (target_inode->fs_stat.st_nlink == 1)
//        {
//            inode.del_dentry(tag_name);
//        }
//        else
//        {
//            target_inode->fs_stat.st_nlink -= 1;
//        }

        return 0;
    }
    catch (stmpfs_error_t & error)
    {
        OBTAIN_STACK_FRAME;
        std::cerr << error.what() << " (errno=" << error.what_errno() << ")" << std::endl;
        if (error.my_errcode() == STMPFS_ERROR_NO_SUCH_FILE_OR_DIRECTORY)
        {
            errno = ENOENT; // No such file or directory (POSIX.1-2001)
        }
        return -errno;
    }
    catch (std::exception & error)
    {
        OBTAIN_STACK_FRAME;
        std::cerr << error.what() << " (errno=" << strerror(errno) << ")" << std::endl;
        return -errno;
    }
}

int do_rmdir (const char * path)
{
    try
    {
        FUNCTION_INFO;

        stmpfs_pathname_t vpath(path);

        if (vpath.get_direct_pathname().empty())
        {
            return -EBUSY;  // Device or resource busy (POSIX.1-2001).
        }

        if (if_is_replicate_path(vpath))
        {
            filesystem_root.replicate_clear();
            return 0;
        }

        if (if_in_replicate_path(vpath))
        {
            return -EROFS;  // Read-only filesystem (POSIX.1-2001)
        }

        std::string tag_name = vpath.get_direct_pathname().back();
        // normal rmdir
        vpath.get_direct_pathname().pop_back();

        auto & inode = pathname_to_inode(vpath, filesystem_root);
        auto * target_inode = inode.find_in_dentry(tag_name);

        if (!(target_inode->fs_stat.st_mode & S_IFDIR))
        {
            return -ENOTDIR; // Not a directory (POSIX.1-2001).
        }

        for (auto & i : target_inode->my_dentry())
        {
            if (!i.second.inode->if_deleted_in_current_version)
            {
                return -ENOTEMPTY; // Directory not empty (POSIX.1-2001).
            }
        }

        // remove directory
        if (target_inode->replicated.if_replicated)
        {
            target_inode->if_deleted_in_current_version = true;
        }
        else
        {
            inode.del_dentry(tag_name);
        }

        return 0;
    }
    catch (stmpfs_error_t & error)
    {
        OBTAIN_STACK_FRAME;
        std::cerr << error.what() << " (errno=" << error.what_errno() << ")" << std::endl;
        if (error.my_errcode() == STMPFS_ERROR_NO_SUCH_FILE_OR_DIRECTORY)
        {
            errno = ENOENT; // No such file or directory (POSIX.1-2001)
        }
        return -errno;
    }
    catch (std::exception & error)
    {
        OBTAIN_STACK_FRAME;
        std::cerr << error.what() << " (errno=" << strerror(errno) << ")" << std::endl;
        return -errno;
    }
}

int do_mknod (const char * path, mode_t mode, dev_t device)
{
    try
    {
        FUNCTION_INFO;

        stmpfs_pathname_t vpath(path);

        if (if_in_replicate_path(vpath))
        {
            return -EROFS;  // Read-only filesystem (POSIX.1-2001)
        }

        std::string tag_name = vpath.get_direct_pathname().back();
        vpath.get_direct_pathname().pop_back();

        auto & inode = pathname_to_inode(vpath, filesystem_root);
        auto tag_inode = inode.my_dentry().find(tag_name);

        inode_t new_inode;

        // fill up info
        auto cur_time = current_time();
        new_inode.fs_stat.st_mode = mode;
        new_inode.fs_stat.st_nlink = 1;
        new_inode.fs_stat.st_atim = cur_time;
        new_inode.fs_stat.st_ctim = cur_time;
        new_inode.fs_stat.st_mtim = cur_time;
        new_inode.fs_stat.st_dev = device;

        if (tag_inode != inode.my_dentry().end())
        {
            if (tag_inode->second.inode->if_deleted_in_current_version)
            {
                tag_inode->second.inode->my_data().clear();
                tag_inode->second.inode->fs_stat = new_inode.fs_stat;
                tag_inode->second.inode->if_deleted_in_current_version = false;
            }
            else // target exists
            {
                return -EEXIST; // File exists (POSIX.1-2001).
            }
        }
        else
        {
            inode.emplace_new_dentry(tag_name, new_inode);
        }


        return 0;
    }
    catch (stmpfs_error_t & error)
    {
        OBTAIN_STACK_FRAME;
        std::cerr << error.what() << " (errno=" << error.what_errno() << ")" << std::endl;
        if (error.my_errcode() == STMPFS_ERROR_NO_SUCH_FILE_OR_DIRECTORY)
        {
            errno = ENOENT; // No such file or directory (POSIX.1-2001)
        }
        return -errno;
    }
    catch (std::exception & error)
    {
        OBTAIN_STACK_FRAME;
        std::cerr << error.what() << " (errno=" << strerror(errno) << ")" << std::endl;
        return -errno;
    }
}

int do_rename (const char * path, const char * name)
{
    try
    {
        FUNCTION_INFO;

        stmpfs_pathname_t src_vpath(path);
        stmpfs_pathname_t dest_vpath(name);

        if (if_in_replicate_path(src_vpath) || if_in_replicate_path(dest_vpath))
        {
            return -EROFS;  // Read-only filesystem (POSIX.1-2001)
        }

        // get src name, pop back
        std::string src_name = src_vpath.get_direct_pathname().back();
        src_vpath.get_direct_pathname().pop_back();

        // get dest name, pop back
        std::string dest_name = dest_vpath.get_direct_pathname().back();
        dest_vpath.get_direct_pathname().pop_back();

        auto & src_parent_inode = pathname_to_inode(src_vpath, filesystem_root);
        auto & dest_parent_inode = pathname_to_inode(dest_vpath, filesystem_root);

        auto src_inode = src_parent_inode.my_dentry().find(src_name);
        auto dest_inode = dest_parent_inode.my_dentry().find(dest_name);

        // find inode
        inode_t * inode = src_parent_inode.find_in_dentry(src_name);

        // remove from source parent
        src_parent_inode.del_dentry(src_name, true);

        if (dest_inode != dest_parent_inode.my_dentry().end())
        {
            if (dest_inode->second.inode->if_deleted_in_current_version)
            {
                dest_inode->second.inode->if_deleted_in_current_version = true;
            }
            else
            {
                // add to destination parent
                dest_parent_inode.add_dentry(dest_name, *inode, 1);
            }
        }

        inode->fs_stat.st_ctim = current_time();

        return 0;
    }
    catch (stmpfs_error_t & error)
    {
        OBTAIN_STACK_FRAME;
        std::cerr << error.what() << " (errno=" << error.what_errno() << ")" << std::endl;
        if (error.my_errcode() == STMPFS_ERROR_NO_SUCH_FILE_OR_DIRECTORY)
        {
            errno = ENOENT; // No such file or directory (POSIX.1-2001)
        }
        return -errno;
    }
    catch (std::exception & error)
    {
        OBTAIN_STACK_FRAME;
        std::cerr << error.what() << " (errno=" << strerror(errno) << ")" << std::endl;
        return -errno;
    }
}

int do_symlink (const char * path, const char * linkname)
{
    try
    {
        FUNCTION_INFO;

        stmpfs_pathname_t vpath(path);

        if (if_in_replicate_path(vpath))
        {
            return -EROFS;  // Read-only filesystem (POSIX.1-2001)
        }

        // get target name
        std::string tag_name = vpath.get_direct_pathname().back();
        vpath.get_direct_pathname().pop_back();

        auto & inode = pathname_to_inode(vpath, filesystem_root);
        auto tag_inode = inode.my_dentry().find(tag_name);

        inode_t new_inode;

        // fill up info
        auto cur_time = current_time();
        new_inode.fs_stat.st_mode = S_IFLNK | 0755;
        new_inode.fs_stat.st_nlink = 1;
        new_inode.fs_stat.st_atim = cur_time;
        new_inode.fs_stat.st_ctim = cur_time;
        new_inode.fs_stat.st_mtim = cur_time;
        new_inode.fs_stat.st_size = (off_t)strlen(linkname);
        new_inode.write(linkname, strlen(linkname), 0);

        if (tag_inode != inode.my_dentry().end())
        {
            if (tag_inode->second.inode->if_deleted_in_current_version)
            {
                tag_inode->second.inode->if_deleted_in_current_version = false;
                tag_inode->second.inode->fs_stat = new_inode.fs_stat;
                tag_inode->second.inode->write(linkname, strlen(linkname), 0);
            }
            else
            {
                return -ENOENT;
            }
        }
        else
        {
            inode.emplace_new_dentry(tag_name, new_inode);
        }

        return 0;
    }
    catch (stmpfs_error_t & error)
    {
        OBTAIN_STACK_FRAME;
        std::cerr << error.what() << " (errno=" << error.what_errno() << ")" << std::endl;
        if (error.my_errcode() == STMPFS_ERROR_NO_SUCH_FILE_OR_DIRECTORY)
        {
            errno = ENOENT; // No such file or directory (POSIX.1-2001)
        }
        return -errno;
    }
    catch (std::exception & error)
    {
        OBTAIN_STACK_FRAME;
        std::cerr << error.what() << " (errno=" << strerror(errno) << ")" << std::endl;
        return -errno;
    }
}

int do_readlink (const char * path, char * buffer, size_t size)
{
    try
    {
        FUNCTION_INFO;

        stmpfs_pathname_t vpath(path);

        if (if_in_replicate_path(vpath))
        {
            vpath_erase_front(vpath);
            auto &inode = pathname_to_inode(vpath, filesystem_root);
            if (inode.replicated.if_replicated)
            {
                inode.replicate_read(buffer, size, 0);
            }
            else
            {
                return -ENOENT;     // No such file or directory (POSIX.1-2001)
            }
        }
        else
        {
            auto &inode = pathname_to_inode(vpath, filesystem_root);
            if (inode.if_deleted_in_current_version)
            {
                return -ENOENT;
            }

            inode.fs_stat.st_atim = current_time();
            inode.read(buffer, size, 0);
        }
        return 0;
    }
    catch (stmpfs_error_t & error)
    {
        OBTAIN_STACK_FRAME;
        std::cerr << error.what() << " (errno=" << error.what_errno() << ")" << std::endl;
        if (error.my_errcode() == STMPFS_ERROR_NO_SUCH_FILE_OR_DIRECTORY)
        {
            errno = ENOENT; // No such file or directory (POSIX.1-2001)
        }
        return -errno;
    }
    catch (std::exception & error)
    {
        OBTAIN_STACK_FRAME;
        std::cerr << error.what() << " (errno=" << strerror(errno) << ")" << std::endl;
        return -errno;
    }
}

int do_ioctl (const char * path, int cmd, void *arg, struct fuse_file_info *, unsigned int flags, void *data)
{
    return -1;
}

int do_truncate (const char * path, off_t size)
{
    try
    {
        FUNCTION_INFO;

        stmpfs_pathname_t vpath(path);

        if (if_in_replicate_path(vpath))
        {
            return -EROFS;  // Read-only filesystem (POSIX.1-2001)
        }

        auto & inode = pathname_to_inode(vpath, filesystem_root);
        if (inode.if_deleted_in_current_version)
        {
            return -ENOENT;
        }

        inode.fs_stat.st_size = size;
        inode.truncate(size);

        return 0;
    }
    catch (stmpfs_error_t & error)
    {
        OBTAIN_STACK_FRAME;
        std::cerr << error.what() << " (errno=" << error.what_errno() << ")" << std::endl;
        if (error.my_errcode() == STMPFS_ERROR_NO_SUCH_FILE_OR_DIRECTORY)
        {
            errno = ENOENT; // No such file or directory (POSIX.1-2001)
        }
        return -errno;
    }
    catch (std::exception & error)
    {
        OBTAIN_STACK_FRAME;
        std::cerr << error.what() << " (errno=" << strerror(errno) << ")" << std::endl;
        return -errno;
    }
}

int do_fallocate(const char * path, int mode, off_t offset, off_t length, struct fuse_file_info *)
{
    try
    {
        FUNCTION_INFO;

        stmpfs_pathname_t vpath(path);

        if (if_in_replicate_path(vpath))
        {
            return -EROFS;  // Read-only filesystem (POSIX.1-2001)
        }

        auto & inode = pathname_to_inode(vpath, filesystem_root);

        if (inode.if_deleted_in_current_version)
        {
            return -ENOENT;
        }

        // fill up info
        auto cur_time = current_time();
        inode.fs_stat.st_mode = mode | S_IFREG;
        inode.fs_stat.st_nlink = 1;
        inode.fs_stat.st_ctim = cur_time;
        inode.fs_stat.st_size = offset + length;
        inode.truncate(offset + length);

        return 0;
    }
    catch (stmpfs_error_t & error)
    {
        OBTAIN_STACK_FRAME;
        std::cerr << error.what() << " (errno=" << error.what_errno() << ")" << std::endl;
        if (error.my_errcode() == STMPFS_ERROR_NO_SUCH_FILE_OR_DIRECTORY)
        {
            errno = ENOENT; // No such file or directory (POSIX.1-2001)
        }
        return -errno;
    }
    catch (std::exception & error)
    {
        OBTAIN_STACK_FRAME;
        std::cerr << error.what() << " (errno=" << strerror(errno) << ")" << std::endl;
        return -errno;
    }
}

int do_fsync (const char * path, int, struct fuse_file_info *)
{
    FUNCTION_INFO;
    return 0;
}

int do_releasedir (const char * path, struct fuse_file_info *)
{
    FUNCTION_INFO;
    return 0;
}

int do_fsyncdir (const char * path, int, struct fuse_file_info *)
{
    FUNCTION_INFO;
    return 0;
}

void inode_setxattr(inode_t & inode, const std::string& name, const char * value, size_t size)
{
    std::string buff;
    for (uint64_t i = 0; i < size; i++)
    {
        buff += value[i];
    }

    inode.xattr[name] = buff;
}

int do_setxattr (const char * path, const char * name, const char * value, size_t size, int flag)
{
    try
    {
        FUNCTION_INFO;

        stmpfs_pathname_t vpath(path);

        if (if_in_replicate_path(vpath))
        {
            return -EROFS;  // Read-only filesystem (POSIX.1-2001)
        }

        auto & inode = pathname_to_inode(vpath, filesystem_root);

        if (inode.if_deleted_in_current_version)
        {
            return -ENOENT;
        }

        if (flag == XATTR_CREATE)
        {
            if (inode.xattr.find(name) != inode.xattr.end())
            {
                return -EEXIST;
            }

            inode_setxattr(inode, name, value, size);
            return 0;
        }
        else if (flag == XATTR_REPLACE)
        {
            if (inode.xattr.find(name) == inode.xattr.end())
            {
                return -ENODATA;
            }

            inode_setxattr(inode, name, value, size);
            return 0;
        }
        else
        {
            inode_setxattr(inode, name, value, size);
        }

        return 0;
    }
    catch (stmpfs_error_t & error)
    {
        OBTAIN_STACK_FRAME;
        std::cerr << error.what() << " (errno=" << error.what_errno() << ")" << std::endl;
        if (error.my_errcode() == STMPFS_ERROR_NO_SUCH_FILE_OR_DIRECTORY)
        {
            errno = ENOENT; // No such file or directory (POSIX.1-2001)
        }
        return -errno;
    }
    catch (std::exception & error)
    {
        OBTAIN_STACK_FRAME;
        std::cerr << error.what() << " (errno=" << strerror(errno) << ")" << std::endl;
        return -errno;
    }
}

/// copy xattr to a name buffer
int copy_to_name_buf(std::map < std::string, std::string > & xattr, const char * name, char * value, size_t size)
{
    auto it = xattr.find(name);
    if (it == xattr.end())
    {
        return -ENODATA;
    }

    if (size == 0 && value == nullptr)
    {
        return (int)it->second.size();
    }

    if (size < it->second.size())
    {
        return -ERANGE;
    }

    for (uint64_t i = 0; i < it->second.size(); i++)
    {
        value[i] = it->second.at(i);
    }

    return (int)it->second.size();
}

int do_getxattr (const char * path, const char * name, char * value, size_t size)
{
    try
    {
        FUNCTION_INFO;

        stmpfs_pathname_t vpath(path);

        if (if_in_replicate_path(vpath))
        {
            vpath_erase_front(vpath);
            auto &inode = pathname_to_inode(vpath, filesystem_root);
            if (inode.replicated.if_replicated)
            {
                return copy_to_name_buf(inode.replicated.xattr, name, value, size);
            }
            else
            {
                return -ENOENT; // No such file or directory (POSIX.1-2001).
            }
        }
        else
        {
            auto & inode = pathname_to_inode(vpath, filesystem_root);
            if (inode.if_deleted_in_current_version)
            {
                return -ENOENT;
            }

            return copy_to_name_buf(inode.xattr, name, value, size);
        }
    }
    catch (stmpfs_error_t & error)
    {
        OBTAIN_STACK_FRAME;
        std::cerr << error.what() << " (errno=" << error.what_errno() << ")" << std::endl;
        if (error.my_errcode() == STMPFS_ERROR_NO_SUCH_FILE_OR_DIRECTORY)
        {
            errno = ENOENT; // No such file or directory (POSIX.1-2001)
        }
        return -errno;
    }
    catch (std::exception & error)
    {
        OBTAIN_STACK_FRAME;
        std::cerr << error.what() << " (errno=" << strerror(errno) << ")" << std::endl;
        return -errno;
    }
}

/// copy str to list buffer
uint64_t copy_to_list(char * list, uint64_t copy_len, const std::string& str)
{
    uint64_t actual_len = MIN(copy_len, str.size());
    for (uint64_t i = 0; i < actual_len; i++)
    {
        list[i] = str.at(i);
    }

    list[actual_len] = 0;
    return actual_len + 1;
}

int copy_to_buffer(std::map < std::string, std::string > & xattr, char * list, size_t list_size)
{
    uint64_t list_actual_size = 0, write_off = 0;
    auto xattr_itr = xattr.begin();
    for (auto & i : xattr)
    {
        list_actual_size += i.first.size() + 1;
    }

    if (list_size == 0 && list == nullptr)
    {
        return (int)list_actual_size;
    }

    if (list_size < list_actual_size)
    {
        return -ERANGE;
    }

    while (write_off < list_actual_size)
    {
        write_off += copy_to_list(list + write_off,
                                  list_actual_size - write_off,
                                  xattr_itr->first);
        xattr_itr++;
    }

    return (int)list_actual_size;
}

int do_listxattr (const char * path, char * list, size_t list_size)
{
    try
    {
        FUNCTION_INFO;

        stmpfs_pathname_t vpath(path);

        if (if_in_replicate_path(vpath))
        {
            vpath_erase_front(vpath);
            auto &inode = pathname_to_inode(vpath, filesystem_root);
            if (inode.replicated.if_replicated)
            {
                return copy_to_buffer(inode.xattr, list, list_size);
            }
            else
            {
                return -ENOENT; // No such file or directory (POSIX.1-2001).
            }
        }
        else
        {
            auto & inode = pathname_to_inode(vpath, filesystem_root);
            if (inode.if_deleted_in_current_version)
            {
                return -ENOENT;
            }

            return copy_to_buffer(inode.xattr, list, list_size);
        }

    }
    catch (stmpfs_error_t & error)
    {
        OBTAIN_STACK_FRAME;
        std::cerr << error.what() << " (errno=" << error.what_errno() << ")" << std::endl;
        if (error.my_errcode() == STMPFS_ERROR_NO_SUCH_FILE_OR_DIRECTORY)
        {
            errno = ENOENT; // No such file or directory (POSIX.1-2001)
        }
        return -errno;
    }
    catch (std::exception & error)
    {
        OBTAIN_STACK_FRAME;
        std::cerr << error.what() << " (errno=" << strerror(errno) << ")" << std::endl;
        return -errno;
    }
}

int do_removexattr (const char * path, const char * name)
{
    try
    {
        FUNCTION_INFO;

        stmpfs_pathname_t vpath(path);

        if (if_in_replicate_path(vpath))
        {
            return -EROFS;  // Read-only filesystem (POSIX.1-2001)
        }

        auto & inode = pathname_to_inode(vpath, filesystem_root);

        if (inode.if_deleted_in_current_version)
        {
            return -ENOENT;
        }

        auto it = inode.xattr.find(name);
        if (it == inode.xattr.end())
        {
            return -ENODATA;
        }

        inode.xattr.erase(it);
        return 0;
    }
    catch (stmpfs_error_t & error)
    {
        OBTAIN_STACK_FRAME;
        std::cerr << error.what() << " (errno=" << error.what_errno() << ")" << std::endl;
        if (error.my_errcode() == STMPFS_ERROR_NO_SUCH_FILE_OR_DIRECTORY)
        {
            errno = ENOENT; // No such file or directory (POSIX.1-2001)
        }
        return -errno;
    }
    catch (std::exception & error)
    {
        OBTAIN_STACK_FRAME;
        std::cerr << error.what() << " (errno=" << strerror(errno) << ")" << std::endl;
        return -errno;
    }
}


