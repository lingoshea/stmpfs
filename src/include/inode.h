#ifndef SMNXFS_INODE_H
#define SMNXFS_INODE_H

/** @file
 *
 * This file defines inode and relevant operations
 */

#include <vector>
#include <sys/types.h>
#include <sys/stat.h>
#include <string>
#include <map>

class inode_t
{
private:
    struct dentry_t
    {
        uint64_t    if_constructed_by_inode:1;  // if this dentry is emplace'd
        inode_t *   inode;
    };

    // replicated data
    struct replicated_t
    {
        uint64_t    if_replicated:1,        // if this inode is replicated (except data)
        if_data_replicated:1;   // if data is replicated
        std::vector < char > data;
        std::map < std::string, dentry_t > dentry;
        struct stat fs_stat { };
        std::map < std::string, std::string > xattr;
    };

    std::vector < char > data;              // if is a file, use this data
    std::map < std::string, dentry_t > dentry; // if is a directory, use this dentry

public:

    bool if_deleted_in_current_version = false;
    replicated_t replicated;            // replicated data
    struct stat fs_stat { };            // file/dir stat, publicly changeable
    std::map < std::string, std::string > xattr; // extended attributes

    /// replicate all inode since current inode
    void replicate_inode();

    /// read from replicate buffer
    /** @param buffer output buffer
     *  @param length length for reading
     *  @param offset read offset **/
    size_t replicate_read(char * buffer, size_t length, off_t offset);

    /// clear replicated content
    void replicate_clear();

    /// read from buffer
    /** @param buffer output buffer
     *  @param length length for reading
     *  @param offset read offset **/
    size_t read(char * buffer, size_t length, off_t offset);

    /// write to buffer
    /** @param buffer output buffer
     *  @param length length for writing
     *  @param offset write offset **/
    size_t write(const char * buffer, size_t length, off_t offset);

    /// clear content
    void clear();

    /// add directory entry
    /** @param name dentry name
     *  @param inode inode **/
    void add_dentry(const std::string& name, inode_t& inode, uint64_t if_alloc_by_inode = 0);

    /// create a new directory entry
    /** @param name dentry name
     *  @param inode inode **/
    void emplace_new_dentry(const std::string& name, const inode_t& inode);

    /// delete directory entry
    /** @param name dentry name
     *  @param protect_child if delete child **/
    void del_dentry(const std::string& name, bool protect_child = false);

    /// find name in next level dentry list
    /** @param name pathname (one level) **/
    inode_t* find_in_dentry(const std::string& name);

    /// get dentry list
    [[nodiscard]] std::map < std::string, dentry_t > & my_dentry () { return dentry; }

    /// get data
    [[nodiscard]] std::vector < char > & my_data () { return data; }

    /// deconstruction
    ~inode_t() { clear(); replicate_clear(); }

    /// construction
    inode_t() noexcept;

    /// change buffer size
    /** @param size target size **/
    void truncate(off_t size);

    inode_t & operator=(const inode_t&&) = delete;
    inode_t(const inode_t &&) = delete;
};


#endif //SMNXFS_INODE_H
