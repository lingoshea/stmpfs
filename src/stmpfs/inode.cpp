/** @file
 *
 * This file implements inode and relevant operations
 */

#include <inode.h>
#include <stmpfs_error.h>

/// read buffer from data
/** @param buffer output buffer
 *  @param length read length
 *  @param offset read offset
 *  @param data input buffer
 *  **/
size_t read_buffer(char * & buffer,
                   size_t & length,
                   off_t & offset,
                   std::vector < char > & data)
{
    if (offset > data.size())
    {
        return 0;
    }

    if ((length + offset) > data.size())
    {
        length = data.size() - offset;
    }

    auto end_point = length + offset;
    // FIXME: low efficiency
    for (size_t i = offset; i < end_point; i++)
    {
        buffer[i - offset] = data.at(i);
    }

    return length;
}

size_t inode_t::read(char *buffer, size_t length, off_t offset)
{
    // read from changeable buffer
    return read_buffer(buffer, length, offset, data);
}

size_t inode_t::write(const char *buffer, size_t length, off_t offset)
{
    // FIXME: low efficiency
    auto current_data_size = data.size();
    if ((offset + length) > current_data_size)
    {
        for (uint64_t i = 0; i < ((offset + length) - current_data_size); i++)
        {
            data.emplace_back(0);
        }
    }

    /// overwrite
    for (size_t i = 0; i < length; i++)
    {
        data[i + offset] = buffer[i];
    }

    fs_stat.st_size = (off_t)data.size();

    return length;
}

void inode_t::clear()
{
    data.clear();
    for (auto & i : dentry)
    {
        if (i.second.if_constructed_by_inode)
        {
            delete i.second.inode;
        }
    }
    dentry.clear();
}

void inode_t::add_dentry(const std::string& name, inode_t& inode, uint64_t if_alloc_by_inode)
{
    auto it = dentry.find(name);
    if (it != dentry.end())
    {
        if (it->second.if_constructed_by_inode)
        {
            delete it->second.inode;
        }

        dentry.erase(it);
    }

    dentry_t new_dentry {
        .if_constructed_by_inode = if_alloc_by_inode,
        .inode = &inode,
    };

    dentry.emplace(name, new_dentry);
}

void inode_t::emplace_new_dentry(const std::string& name, const inode_t& inode)
{
    auto it = dentry.find(name);
    if (it != dentry.end())
    {
        if (it->second.if_constructed_by_inode)
        {
            delete it->second.inode;
        }

        dentry.erase(it);
    }

    dentry_t new_dentry
    {
        .if_constructed_by_inode = 1,
        .inode = new inode_t,
    };

    new_dentry.inode->fs_stat = inode.fs_stat;
    new_dentry.inode->dentry = inode.dentry;
    new_dentry.inode->data = inode.data;

    dentry.emplace(name, new_dentry);
}

void inode_t::del_dentry(const std::string& name, bool protect_child)
{
    auto it = dentry.find(name);
    if (it != dentry.end())
    {
        if (it->second.if_constructed_by_inode && !protect_child)
        {
            delete it->second.inode;
        }

        dentry.erase(it);
        return;
    }

    throw stmpfs_error_t(STMPFS_ERROR_NO_SUCH_FILE_OR_DIRECTORY);
}

inode_t *inode_t::find_in_dentry(const std::string &name)
{
    auto it = dentry.find(name);
    if (it != dentry.end())
    {
        return it->second.inode;
    }

    throw stmpfs_error_t(STMPFS_ERROR_NO_SUCH_FILE_OR_DIRECTORY);
}

inode_t::inode_t() noexcept = default;

void inode_t::truncate(off_t size)
{
    if (size < data.size())
    {
        auto loop_count = data.size() - size;
        for (uint64_t i = 0; i < loop_count; i++)
        {
            data.pop_back();
        }
    }
    else if (size > data.size())
    {
        auto loop_count = size - data.size();
        for (uint64_t i = 0; i < loop_count; i++)
        {
            data.push_back(0);
        }
    }
}
